<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

session_start();

require '../vendor/autoload.php';
require '../src/main/php/config.php';
require '../src/main/php/routes.php';
require '../src/main/php/dependencies.php';


\ORM::configure($appConfig['settings']['db']['dsn']);
\ORM::configure('username', $appConfig['settings']['db']['username']);
\ORM::configure('password', $appConfig['settings']['db']['password']);

if(substr($appConfig['settings']['db']['dsn'], 0, 6) === 'mysql:') {
    \ORM::configure('driver_options', [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
}

$app = new \Slim\App($appConfig);

initializeDependencies($app);
initializeRoutes($app);

$app->run();
