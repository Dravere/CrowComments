# Databases

At the moment only the MariaDB is tested. Other databases are likely not to work properly. Before initial release support for MySQL, PostgreSQL and SQLite might be added. In this document important differences to the other databases are collected as they come up during development that later can be addressed.

## Case Insensitivity

The `user` table must have an unique case insensitive index on name and e-mail. This seems to be rather difficult to get right on different databases.

### MySQL & MariaDB

On MySQL and MariaDB the easiest to implement this is via a collation on the column. At the moment a collation of `utf8mb4_unicode_ci` is used as a default. This already guarantees the case insensitivity of the column in the index.

### PostgreSQL

On PostgreSQL the index should be on either the lower, upper or upper to lower case of the actual column. Thus the unique constraint might look something like: `CREATE UNIQUE users_name_uindex ON users (LOWER(UPPER(name)));`

It is important to note that the constraint is added via `CREATE UNIQUE` as the SQL standard doesn't allow an expression in the constraint of create or alter table. Also this might still produce some wrong results in some languages. It is definitely not the best solution.

Going to upper will resolve problems like for the german lower ß to upper SS. Another example can be found with the greek Sigma. The upper case of Sigma is Σ and the lower case is normally σ and at the end of a word it is ς.

Going back to lower case is done in the hope that similar issues will be fixed in other languages. Though none are known to the author of this document yet.

### SQLite

Case insensitivity can be regarded as a major problem in SQLite. There is basically no real possibility to introduce it. It is a possibility to add a column constraint for a case insensitive collation `COLLATE NOCASE`. But the problem is that it only applies for [ASCII characters](https://sqlite.org/datatype3.html#collating_sequences). The same applies to the `upper` and `lower` string functions.

There is a possibility to provide your own collation in SQLite. And it is implemented as an [experimental feature](http://ch1.php.net/manual/en/pdo.sqlitecreatecollation.php) in php. It also might be a bit slow. But apparently there is no correct unicode string case insensitive comparison in php. Thus only something similar to the PostgreSQL solution can be implemented.

```php
$pdo->sqliteCreateCollation('NOCASEUTF8', function ($left, $right) {
    return strcmp(
        mb_strtolower(mb_strtoupper($left, 'UTF-8'), 'UTF-8'),
        mb_strtolower(mb_strtoupper($right, 'UTF-8'), 'UTF-8'));
});
```

This is not the best solution for the same reasons as mentioned in PostgreSQL. It might not work correctly for all languages.