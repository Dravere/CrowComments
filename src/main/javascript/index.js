import Vue from 'vue';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import '../less/main.less';

document.addEventListener('DOMContentLoaded', function () {
    // loads the Icon plugin
    // UIkit.use(Icons);

// components can be called from the imported UIkit reference
    UIkit.notification('<span uk-icon="heart"></span> Hello world <span uk-icon="heart"></span>');
});


// new Vue({});
