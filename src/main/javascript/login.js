import Vue from 'vue';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import axios from 'axios';

import apiErrorHandler from './modules/apiErrors';

/* global: basePath */

document.addEventListener('DOMContentLoaded', function () {
    // loads the Icon plugin
    UIkit.use(Icons);

    new Vue({
        el: '#app',
        data: {
            email: '',
            password: '',
            loggingIn: false
        },
        computed: {
            emailValid: function () {
                if(this.email.length === 0) {
                    return null;
                }

                return this.email.length >= 3 && this.email.indexOf('@') > 0;
            },
            loginPossible: function () {
                return this.emailValid && this.password.length >= 12;
            }
        },
        methods: {
            login: async function () {
                this.loggingIn = true;
                try {
                    await axios.post(basePath + '/api/login', {email: this.email, password: this.password});
                    window.location.href = basePath + '/admin/';
                } catch (error) {
                    apiErrorHandler(error, [{
                        test: /^email/,
                        data: this.emailAlerts
                    }, {
                        test: /^password/,
                        data: this.passwordAlerts
                    }]);
                } finally {
                    this.loggingIn = false;
                }
            }
        }
    });
});
