import Vue from 'vue';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import logout from "./modules/logout"

import AdminSites from './components/admin-sites';

document.addEventListener('DOMContentLoaded', function () {
    // loads the Icon plugin
    UIkit.use(Icons);

    new Vue({
        el: '#app',
        components: {
            AdminSites
        },
        methods: {
            logout
        }
    });
});
