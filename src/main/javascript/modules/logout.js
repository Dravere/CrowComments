import axios from "axios";

import apiErrorHandler from "./apiErrors";


export default async function () {
    try {
        await axios.post(basePath + '/api/logout', {});
        window.location.href = basePath + '/login';
    } catch (error) {
        apiErrorHandler(error);
    }
}
