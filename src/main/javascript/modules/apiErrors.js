import UIkit from 'uikit';

export default function (error, validationHandling=[]) {
    let message = '';
    if(error.response) {
        if(error.response.status === 400) {
            message = error.response.data.error;

            if(error.response.data.validations && Array.isArray(validationHandling) && validationHandling.length > 0) {
                for(const validation of error.response.data.validations) {
                    for(const handler of validationHandling) {
                        if(handler.test.test(validation)) {
                            if(Array.isArray(handler.data)) {
                                handler.data.push(validation);
                            } else if(typeof handler.data === 'function') {
                                handler.data(validation);
                            }

                            break;
                        }
                    }
                }
            }
        } else if(error.response.status > 400 && error.response.status < 500) {
            message = error.response.data.error;
        } else {
            message = 'An unexpected error occurred. Try again later or contact support!';
        }
    } else if(error.request) {
        message = 'Request timed out. Try again later.';
    } else {
        message = 'Failed to send request: ' + error.message;
    }

    UIkit.notification(message, {status: 'danger'});
}