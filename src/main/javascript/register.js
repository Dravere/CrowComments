import Vue from 'vue';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import axios from 'axios';

import apiErrorHandler from './modules/apiErrors';

/* global: basePath */

document.addEventListener('DOMContentLoaded', function () {
    // loads the Icon plugin
    UIkit.use(Icons);

    const app = new Vue({
        el: '#app',
        data: {
            email: '',
            password: '',
            confirmPassword: '',
            submitting: false,
            emailAlerts: [],
            passwordAlerts: [],
            emailSuccess: null,
            passwordSuccess: null
        },
        computed: {
            confirmSuccess: function () {
                if(this.password.length === 0 && this.confirmPassword.length === 0) {
                    return null;
                }

                return this.password === this.confirmPassword;
            },
            submitPossible: function () {
                return this.email.length >= 3 && this.email.indexOf('@') > 0 && this.confirmSuccess === true;
            }
        },
        methods: {
            submitRegistration: async function () {
                this.submitting = true;
                try {
                    await axios.post(basePath + '/api/register', {email: this.email, password: this.password});
                    window.location.href = basePath + '/';
                } catch (error) {
                    this.emailAlerts = [];
                    this.passwordAlerts = [];
                    apiErrorHandler(error, [{
                        test: /^email/,
                        data: this.emailAlerts
                    }, {
                        test: /^password/,
                        data: this.passwordAlerts
                    }]);
                } finally {
                    this.submitting = false;
                    this.emailSuccess = this.emailAlerts.length === 0;
                    this.passwordSuccess = this.passwordSuccess === 0;
                }
            }
        }
    });
});
