import Vue from 'vue';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

import logout from './modules/logout';
import AdminThreads from './components/admin-threads'


document.addEventListener('DOMContentLoaded', function () {
    // loads the Icon plugin
    UIkit.use(Icons);

    new Vue({
        el: '#app',
        components: {
            AdminThreads
        },
        methods: {
            logout
        }
    });
});
