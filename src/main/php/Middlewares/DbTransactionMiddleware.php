<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Middlewares;


use Slim\Http\Request;
use Slim\Http\Response;


class DbTransactionMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        \ORM::getDb()->beginTransaction();

        /** @noinspection PhpUnusedLocalVariableInspection */
        $commit = false;

        try {
            $result = $next($request, $response);
            $commit = true;
            return $result;
        }
        finally {
            if($commit) {
                \ORM::getDb()->commit();
            }
            else {
                \ORM::getDb()->rollBack();
            }
        }
    }
}
