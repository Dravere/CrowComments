<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Middlewares;


use FMGSoftware\CrowComments\Controllers\Utilities;
use FMGSoftware\CrowComments\Models\User;
use FMGSoftware\CrowComments\Session;
use Slim\Http\Request;
use Slim\Http\Response;


class UserMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $user = User::findOne(Session::getUserId());
        if($user === false) {
            return Utilities::commonErrorResponse(
                $response,
                "You need to login to access this page!",
                403);
        }

        return $next($request->withAttribute('user', $user), $response);
    }
}