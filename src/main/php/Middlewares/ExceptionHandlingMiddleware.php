<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Middlewares;


use FMGSoftware\CrowComments\Controllers\Utilities;
use FMGSoftware\CrowComments\UnexpectedException;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Respect\Validation\Exceptions\NestedValidationException;
use Slim\Http\Request;
use Slim\Http\Response;


class ExceptionHandlingMiddleware
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, callable $next)
    {
        try {
            return $next($request, $response);
        }
        catch(NestedValidationException $exception) {
            return Utilities::commonValidationResponse($response, $exception->getMessages(), 400);
        }
        catch(UnexpectedException $exception) {
            $this->getLogger()->critical($exception->getMessage(), $exception->getData());
            return Utilities::commonErrorResponse($response, $exception->getMessage(), 500);
        }
    }

    private function getLogger(): Logger
    {
        return $this->container->get('logger');
    }
}
