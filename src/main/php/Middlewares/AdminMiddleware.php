<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Middlewares;


use FMGSoftware\CrowComments\Controllers\AuthController;
use Slim\Http\Request;
use Slim\Http\Response;


class AdminMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if(!AuthController::isAdmin()) {
            if(\strpos($request->getHeaderLine('Accept'), 'text/html') !== false) {
                return $response->withRedirect('/login', 302);
            }

            return $response->withJson([
                'error' => 'Not authorized'
            ], 401);
        }

        return $next($request, $response);
    }
}
