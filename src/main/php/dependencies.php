<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

function initializeDependencies(\Slim\App $app) {
    $container = $app->getContainer();

    $container['view'] = function(\Slim\Container $c) {
        $view = new \Slim\Views\Twig(\realpath(\dirname(__FILE__) . '/../twig/'), [
            // TODO Switch for production: 'cache' => \realpath(\dirname(__FILE__) . '/../../../temp/twig-cache')
        ]);

        // Instantiate and add Slim specific extension
        $router = $c->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

        return $view;
    };

    $container['logger'] = function(\Slim\Container $c) {
        $logger = new \Monolog\Logger('CrowCommentsLogger');
        $file = dirname(dirname(dirname(dirname(__FILE__)))) . '/logs/app.log';
        $fileHandler = new \Monolog\Handler\StreamHandler($file);
        $logger->pushHandler($fileHandler);
        return $logger;
    };
}