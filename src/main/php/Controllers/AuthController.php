<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Controllers;


use FMGSoftware\CrowComments\Models\SitesModerators;
use FMGSoftware\CrowComments\Models\User;
use FMGSoftware\CrowComments\Session;
use FMGSoftware\CrowComments\UnexpectedException;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Respect\Validation\Validator as v;


class AuthController
{
    public static function isAdmin()
    {
        $user = User::findOne(Session::getUserId());

        return $user !== false && \intval($user->isAdmin) === 1;
    }

    public static function isModerator(int $siteId)
    {
        return SitesModerators::isModeratorForSite(Session::getUserId(), $siteId);
    }

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function login(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if(!isset($data['email']) || !isset($data['password'])) {
            return $this->loginFailed($response);
        }

        $user = User::findUserByEmail($data['email']);
        if($user === false) {
            return $this->loginFailed($response);
        }

        if(\password_verify($data['password'], $user->password) !== true) {
            return $this->loginFailed($response);
        }

        $userId = $user->id;
        $this->getLogger()->info("User ($userId) logged in!");
        Session::setUserId($userId);

        return Utilities::commonResponse($response, [], 200);
    }

    private function loginFailed($response)
    {
        return Utilities::commonErrorResponse(
            $response,
            "No matching user and password found!",
            403);
    }

    public function logout(Request $request, Response $response)
    {
        $userId = Session::getUserId();
        if(!\is_null($userId)) {
            $this->getLogger()->info("User ($userId) logged out!");
            Session::deleteUserId();
        }

        return Utilities::commonResponse($response, [], 200);
    }

    public function register(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        $validator = v::key('password', v::stringType()->length(12, 512))
            ->key('email', v::stringType()->length(3, 255)->email());

        $validator->assert($data);
        $password = $data['password'];
        $email = $data['email'];

        $hashedPassword = \password_hash($password, PASSWORD_DEFAULT);
        if($hashedPassword === false) {
            throw new UnexpectedException(
                "Failed to properly hash the password! Aborting!",
                ['email' => $email]);
        }

        $user = User::findUserByEmail($email);
        if($user === false) {
            $user = User::create();
            $user->email = $email;
            $user->recordTime = \time();
        }
        else if(!\is_null($user->registerTime)) {
            return Utilities::commonErrorResponse(
                $response,
                "An account is already registered for this e-mail address!",
                400);
        }

        $user->registerTime = \time();
        $user->password = $hashedPassword;
        if($user->save() !== true) {
            throw new UnexpectedException(
                "Unable to create the user entry for unknown reasons!",
                ['email' => $email]);
        }

        $userId = $user->id();
        Session::setUserId($userId);
        $this->getLogger()->info("Implicit login for user ($userId) after registration!");

        return Utilities::commonResponse($response, ['userId' => $userId], 200);
    }

    public function egoUpdate(Request $request, Response $response)
    {
        // Update for e-mail
        $userId = Session::getUserId();
        $user = User::findOne($userId);
        if($user === false) {
            throw new UnexpectedException(
                "You don't exist and thus can't update yourself!",
                ['userId' => $userId]);
        }

        $data = $request->getParsedBody();
        if(\password_verify($data['password'], $user->password) !== true) {
            return Utilities::commonErrorResponse(
                $response,
                "The provided password doesn't match!",
                403);
        }

        User::validateUserData($data, $userId);
        $email = $data['email'];
        $name = isset($data['name']) ? $data['name'] : null;

        $user->name = $name;
        $user->email = $email;
        if($user->save() !== true) {
            throw new UnexpectedException(
                "Failed to update the user data for unknown reasons!",
                ['userId' => $userId, 'email' => $email]);
        }

        return Utilities::commonResponse($response, ['userId' => $userId], 200);
    }

    public function egoPassword(Request $request, Response $response)
    {
        // Update for password
        $userId = Session::getUserId();
        $user = User::findOne($userId);
        if($user === false) {
            throw new UnexpectedException(
                "You don't exist and thus can't update yourself!",
                ['userId' => $userId]);
        }

        $data = $request->getParsedBody();
        if(\password_verify($data['password'], $user->password) !== true) {
            return Utilities::commonErrorResponse(
                $response,
                "The provided password doesn't match!",
                403);
        }

        $password = $data['newPassword'];
        if($password !== $data['newPasswordConfirm']) {
            return Utilities::commonErrorResponse(
                $response,
                "The new password and its confirmation do not match!",
                400);
        }

        $passwordValidator = v::stringType()->length(8, 200);
        $passwordValidator->assert($password);

        $hashedPassword = \password_hash($password, PASSWORD_DEFAULT);
        if($hashedPassword === false) {
            throw new UnexpectedException(
                "Failed to properly hash the password! Aborting!",
                ['userId' => $userId]);
        }

        $user->password = $hashedPassword;
        if($user->save() !== true) {
            throw new UnexpectedException(
                "Failed to update the user password for unknown reasons!",
                ['userId' => $userId]);
        }

        return Utilities::commonResponse($response, ['userId' => $userId], 200);
    }

    /**
     * @return Logger
     */
    private function getLogger()
    {
        return $this->container->get('logger');
    }
}
