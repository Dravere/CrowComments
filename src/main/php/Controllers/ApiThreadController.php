<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Controllers;


use FMGSoftware\CrowComments\Models\Comment;
use FMGSoftware\CrowComments\Models\Thread;
use FMGSoftware\CrowComments\Models\User;
use FMGSoftware\CrowComments\Pagination;
use FMGSoftware\CrowComments\Session;
use FMGSoftware\CrowComments\UnexpectedException;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;


class ApiThreadController
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getComments(Request $request, Response $response, array $args)
    {
        $pagination = Pagination::createFromRequest($request);

        if(!isset($args['threadToken'])) {
            return Utilities::commonErrorResponse($response, 'No thread token provided!', 400);
        }

        $thread = Thread::getThreadByToken($args['threadToken']);
        if($thread === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No thread with the given token '${args['threadToken']}' found!",
                404);
        }

        $comments = Comment::startQuery($pagination)
            ->whereEqual(Comment::THREAD_ID_COLUMN, $thread->id)
            ->findMany();

        $data = Utilities::map($comments);
        $total = Comment::startQuery()->whereIdIs($thread->id)->count();

        return Utilities::commonListResponse($response, $total, $data);
    }

    public function createComment(Request $request, Response $response, array $args)
    {
        // TODO Look if we are admin, prevent comment if admin mail is used
        $threadToken = $args['threadToken'];
        $thread = Thread::getThreadByToken($threadToken);
        if($thread === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No thread with the token '$threadToken' found!",
                404);
        }

        $data = $request->getParsedBody();
        Comment::validateCommentData($data, Session::getUserId());

        $comment = Comment::create();
        $comment->userHandle = $data['userHandle'];
        $comment->message = $data['message'];
        $comment->threadId = $thread->id;

        $email = $data['email'];
        $userId = User::findOrCreate($email);
        if($userId === false) {
            throw new UnexpectedException(
                "Failed to set the ownership of the comment for unknown reasons!",
                $data);
        }

        $comment->userId = $userId;
        if($comment->save() !== true) {
            throw new UnexpectedException(
                "Failed to save the comment for unknown reasons!",
                $data);
        }

        Session::push('commentAuthor', $comment->id());

        return Utilities::commonResponse($response, [
            'id' => $comment->id()
        ], 201);
    }

    public function updateComment(Request $request, Response $response, array $args)
    {
        $commentId = $args['commentId'];

        $comment = Comment::findOne($commentId);
        if($comment === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No comment with the id '$commentId' found!",
                404);
        }

        if(Session::contains('commentAuthor', $commentId) !== true) {
            return Utilities::commonErrorResponse(
                $response,
                "You don't have the rights to edit this comment!",
                401);
        }

        $data = $request->getParsedBody();
        Comment::validateCommentMessage($data);
        $comment->message = $data['message'];

        if($comment->save() !== true) {
            throw new UnexpectedException(
                "Failed to update the comment for unknown reasons!",
                ['id' => $commentId, 'data' => $data]);
        }

        return Utilities::commonResponse(
            $response,
            ['data' => $comment->toMap()]);
    }
}
