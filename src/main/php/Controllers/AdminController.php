<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Controllers;


use FMGSoftware\CrowComments\Models\Comment;
use FMGSoftware\CrowComments\Models\Site;
use FMGSoftware\CrowComments\Models\Thread;
use FMGSoftware\CrowComments\Models\User;
use FMGSoftware\CrowComments\Models\UserHandleRule;
use FMGSoftware\CrowComments\Pagination;
use FMGSoftware\CrowComments\Session;
use FMGSoftware\CrowComments\UnexpectedException;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;


class AdminController
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getSiteList(Request $request, Response $response)
    {
        $pagination = Pagination::createFromRequest($request);
        $data = Utilities::map(Site::startQuery($pagination)->findMany());

        return Utilities::commonListResponse(
            $response,
            Site::startQuery()->count(),
            $data);
    }

    public function getSite(Request $request, Response $response, $args)
    {
        $siteId = Utilities::getObjectId($args, 'siteId');
        $site = Site::findOne($siteId);
        if($site === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Site with the id '${args['siteId']}' not found!",
                404);
        }

        return Utilities::commonResponse(
            $response,
            ['data' => $site->toMap()]);
    }

    public function getSiteThreadList(Request $request, Response $response, array $args)
    {
        $siteId = Utilities::getObjectId($args, 'siteId');
        $pagination = Pagination::createFromRequest($request);

        $total = Thread::startQuery()->whereEqual(Thread::SITE_ID_COLUMN, $siteId)->count();

        $data = Utilities::map(
            Thread::startQuery($pagination)
                ->whereEqual(Thread::SITE_ID_COLUMN, $siteId)
                ->findMany());

        return Utilities::commonResponse(
            $response,
            ['total' => $total, 'data' => $data]);
    }

    public function getThreadList(Request $request, Response $response)
    {
        $pagination = Pagination::createFromRequest($request);

        return Utilities::commonListResponse(
            $response,
            Thread::startQuery()->count(),
            Utilities::map(Thread::startQuery($pagination)->findMany()));
    }

    public function getThread(Request $request, Response $response, array $args)
    {
        $threadId = Utilities::getObjectId($args, 'threadId');
        $thread = Thread::findOne($threadId);
        if($thread === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Thread with the id '${args['threadId']}' not found!",
                404);
        }

        return Utilities::commonResponse(
            $response,
            ['data' => $thread->toMap()]);
    }

    public function getCommentList(Request $request, Response $response)
    {
        $pagination = Pagination::createFromRequest($request);

        return Utilities::commonListResponse(
            $response,
            Comment::startQuery()->count(),
            Utilities::map(Comment::startQuery($pagination)->findMany()));
    }

    public function getComment(Request $request, Response $response, array $args)
    {
        $commentId = Utilities::getObjectId($args, 'commentId');
        $comment = Comment::findOne($commentId);
        if($comment === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Comment with the id '${args['commentId']}' not found!",
                404);
        }

        return Utilities::commonResponse(
            $response,
            ['data' => $comment->toMap()]);
    }

    public function getUserList(Request $request, Response $response)
    {
        $pagination = Pagination::createFromRequest($request);
        return Utilities::commonListResponse(
            $response,
            User::startQuery()->count(),
            Utilities::map(User::startQuery($pagination)->findMany(), function(User $user) {
                $mapped = $user->toMap();
                $mapped['commentCount'] = Comment::startQuery()->whereEqual(Comment::USER_ID_COLUMN, $user->id)->count();
                return $mapped;
            }));
    }

    public function getUser(Request $request, Response $response, array $args)
    {
        $userId = Utilities::getObjectId($args, 'userId');
        $user = User::findOne($userId);
        if($user === false) {
            return Utilities::commonErrorResponse(
                $response,
                "User with the id '${args['userId']}' not found!",
                404);
        }

        return Utilities::commonResponse(
            $response,
            ['data' => $user->toMap()]);
    }

    public function getUserHandleRuleList(Request $request, Response $response)
    {
        $pagination = Pagination::createFromRequest($request);
        return Utilities::commonListResponse(
            $response,
            UserHandleRule::startQuery()->count(),
            Utilities::map(UserHandleRule::startQuery($pagination)->findMany()));
    }

    public function createSite(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        Site::validateSiteData($data);
        $site = Site::create();
        $site->caption = $data['caption'];
        $site->url = $data['url'];

        if($site->save() !== true) {
            throw new UnexpectedException(
                "Failed to create site for unknown reasons!",
                $data);
        }

        return Utilities::commonResponse($response, ['siteId' => $site->id()], 201);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function createThread(Request $request, Response $response, array $args)
    {
        $siteId = Utilities::getObjectId($args, 'siteId');
        if(!Site::exists($siteId)) {
            return Utilities::commonErrorResponse(
                $response,
                "Site with the id '${args['siteId']}' doesn't exist!",
                404);
        }

        $data = $request->getParsedBody();
        Thread::validateThreadData($data);
        $thread = Thread::create();

        $thread->caption = $data['caption'];
        $thread->siteId = $siteId;

        if(isset($data['token'])) {
            $token = $data['token'];
        }
        else {
            $token = Thread::generateUniqueToken();
            if($token === false) {
                throw new UnexpectedException('Failed to generate unique thread token!');
            }
        }

        $thread->token = $token;
        if($thread->save() !== true) {
            throw new UnexpectedException(
                "Unknown error happened while updating the site!",
                ['siteId' => $siteId, 'data' => $data]);
        }

        return Utilities::commonResponse($response, [
            'threadId' => $thread->id(),
            'token' => $token
        ], 201);
    }

    public function createUserHandleRule(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        UserHandleRule::validateRuleData($data);

        $rule = UserHandleRule::create();
        $rule->name = $data['name'];
        $rule->regex = $data['regex'];
        $rule->userMessage = $data['userMessage'];
        $rule->description = $data['description'];
        $rule->entryTime = \time();

        if($rule->save() !== true) {
            throw new UnexpectedException(
                "Unknown error happened while saving the user handle rule!",
                $data);
        }

        return Utilities::commonResponse($response, ['ruleId' => $rule->id()], 201);
    }

    public function updateSite(Request $request, Response $response, array $args)
    {
        $siteId = Utilities::getObjectId($args, 'siteId');
        $site = Site::findOne($siteId);
        if($site === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Site with the id '${args['siteId']} doesn't exist!",
                404);
        }

        $data = $request->getParsedBody();
        Site::validateSiteData($data);
        $site->caption = $data['caption'];
        $site->url = $data['url'];

        if($site->save() !== true) {
            throw new UnexpectedException(
                "Unknown error happened while updating the site!",
                ['siteId' => $siteId, 'data' => $data]);
        }

        return Utilities::commonResponse($response, ['siteId' => $siteId]);
    }

    public function updateThread(Request $request, Response $response, array $args)
    {
        $threadId = Utilities::getObjectId($args, 'threadId');
        $thread = Thread::findOne($threadId);
        if($thread === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Thread with the id '${args['threadId']}' doesn't exist!",
                404);
        }

        $data = $request->getParsedBody();
        Thread::validateThreadData($data, $threadId);
        $thread->caption = $data['caption'];

        if(isset($data['token'])) {
            $token = $data['token'];
        }
        else {
            $token = Thread::generateUniqueToken();
            if($token === false) {
                throw new UnexpectedException('Failed to generate unique thread token!');
            }
        }

        $thread->token = $token;

        if($thread->save() !== true) {
            throw new UnexpectedException(
                "Unknown error happened while updating the thread!",
                ['threadId' => $threadId, 'data' => $data]);
        }

        return Utilities::commonResponse($response, ['threadId' => $threadId]);
    }

    public function updateComment(Request $request, Response $response, array $args)
    {
        $commentId = Utilities::getObjectId($args, 'commentId');

        $comment = Comment::findOne($commentId);
        if($comment === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Comment with the id ${args['commentId']} doesn't exist!",
                404);
        }

        $data = $request->getParsedBody();
        Comment::validateCommentData($data, $comment->userId);
        $comment->userHandle = $data['userHandle'];
        $comment->message = $data['message'];

        $email = $data['email'];
        $userId = User::findOrCreate($email);
        if($userId === false) {
            throw new UnexpectedException(
                "Failed to update the ownership of the comment for unknown reasons",
                $data);
        }

        $comment->userId = $userId;

        if($comment->save() !== true) {
            throw new UnexpectedException(
                "Unknown error happened while updating the comment!",
                ['commentId' => $commentId, 'data' => $data]);
        }

        return Utilities::commonResponse($response, ['commentId' => $commentId], 200);
    }

    public function updateUser(Request $request, Response $response, array $args)
    {
        $userId = Utilities::getObjectId($args, 'userId');
        $user = User::findOne($userId);
        if($user === false) {
            return Utilities::commonErrorResponse(
                $response,
                "User with the id ${args['userId']} doesn't exist!",
                404);
        }

        $data = $request->getParsedBody();
        User::validateUserData($data, $userId);
        $email = $data['email'];
        $name = isset($data['name']) ? $data['name'] : null;

        $user->name = $name;
        $user->email = $email;
        if($user->save() !== true) {
            throw new UnexpectedException(
                "Failed to update the user data for unknown reasons!",
                ['userId' => $userId, 'email' => $email]);
        }

        return Utilities::commonResponse($response, ['userId' => $userId], 200);
    }

    public function updateUserHandleRule(Request $request, Response $response, array $args)
    {
        $ruleId = Utilities::getObjectId($args, 'ruleId');

        $rule = UserHandleRule::findOne($ruleId);
        if($rule === false) {
            return Utilities::commonErrorResponse(
                $response,
                "Rule with the id ${args['ruleId']} doesn't exist!",
                404);
        }

        $data = $request->getParsedBody();
        UserHandleRule::validateRuleData($data);

        $rule->name = $data['name'];
        $rule->regex = $data['regex'];
        $rule->userMessage = $data['userMessage'];
        $rule->description = $data['description'];

        if($rule->save() !== true) {
            throw new UnexpectedException(
                "Unknown error happened while updating the user handle rule!",
                ['ruleId' => $ruleId, 'data' => $data]);
        }

        return Utilities::commonResponse($response, ['ruleId' => $ruleId], 200);
    }

    public function deleteSite(Request $request, Response $response, array $args)
    {
        $siteId = Utilities::getObjectId($args, 'siteId');
        $site = Site::findOne($siteId);
        if($site === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No site with id '${args['siteId']}' found to delete!",
                404);
        }

        if($site->delete() !== true) {
            throw new UnexpectedException(
                "Failed to delete site for unknown reasons!",
                ['siteId' => $siteId]);
        }

        return Utilities::commonResponse($response, ['siteId' => $siteId]);
    }

    public function deleteThread(Request $request, Response $response, array $args)
    {
        $threadId = Utilities::getObjectId($args, 'threadId');
        $thread = Thread::findOne($threadId);
        if($thread === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No thread with id '${args['threadId']}' found to delete!",
                404);
        }

        if($thread->delete() !== true) {
            throw new UnexpectedException(
                "Failed to delete thread for unknown reasons!",
                ['threadId' => $threadId]);
        }

        return Utilities::commonResponse($response, ['threadId' => $threadId]);
    }

    public function deleteComment(Request $request, Response $response, array $args)
    {
        $commentId = Utilities::getObjectId($args, 'commentId');
        $comment = Comment::findOne($commentId);
        if($comment === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No comment with the id '${args['commentId']}' found!",
                404);
        }

        if($comment->delete() !== true) {
            throw new UnexpectedException(
                "Failed to delete the comment for unknown reasons!",
                ['commentId' => $commentId]);
        }

        return Utilities::commonResponse($response, ['commentId' => $commentId]);
    }

    public function deleteUser(Request $request, Response $response, array $args)
    {
        $userId = Utilities::getObjectId($args, 'userId');
        $user = User::findOne($userId);
        if($user === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No user with id '${args['userId']}' found to delete!",
                404);
        }

        if($userId === Session::getUserId()) {
            return Utilities::commonErrorResponse(
                $response,
                "You can't delete yourself here. Please do so through your profile!",
                400);
        }

        if($user->delete() !== true) {
            throw new UnexpectedException(
                "Failed to delete user for unknown reasons!",
                ['userId' => $userId]);
        }

        return Utilities::commonResponse($response,['userId' => $userId]);
    }

    public function deleteUserHandleRule(Request $request, Response $response, array $args)
    {
        $ruleId = Utilities::getObjectId($args, 'ruleId');
        $rule = UserHandleRule::findOne($ruleId);
        if($rule === false) {
            return Utilities::commonErrorResponse(
                $response,
                "No user handle rule with id '${args['ruleId']}' found to delete!",
                404);
        }

        if($rule->delete() !== true) {
            throw new UnexpectedException(
                "Failed to delete user handle rule for unknown reasons!",
                ['ruleId' => $ruleId]);
        }

        return Utilities::commonResponse($response, ['ruleId' => $ruleId]);
    }

//    private function getLogger(): Logger
//    {
//        return $this->container->get('logger');
//    }
}