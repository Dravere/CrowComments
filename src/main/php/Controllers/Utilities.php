<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Controllers;


use FMGSoftware\CrowComments\Models\CrowModel;
use Slim\Http\Response;


class Utilities
{
    /**
     * @param Response $response
     * @param array $additionalData
     * @param null $status
     * @return Response
     */
    public static function commonResponse(Response $response, array $additionalData, $status = null)
    {
        return $response->withJson(\array_merge(['timestamp' => time()], $additionalData), $status);
    }

    /**
     * @param Response $response
     * @param int $total
     * @param array $data
     * @param null $status
     * @return Response
     */
    public static function commonListResponse(Response $response, int $total, array $data, $status = null)
    {
        return self::commonResponse($response, ['total' => $total, 'data' => $data], $status);
    }

    /**
     * @param Response $response
     * @param string $message
     * @param null $status
     * @param array $additionalData
     * @return Response
     */
    public static function commonErrorResponse(
        Response $response,
        string $message,
        $status = null,
        array $additionalData = [])
    {
        $status = \is_null($status) ? 400 : $status;
        return self::commonResponse($response, \array_merge(['error' => $message], $additionalData), $status);
    }

    /**
     * @param Response $response
     * @param array $validationErrors
     * @param null $status
     * @return Response
     */
    public static function commonValidationResponse(Response $response, array $validationErrors, $status = null)
    {
        $status = \is_null($status) ? 400 : $status;
        return self::commonErrorResponse(
            $response,
            'Validation failed!',
            $status,
            ['validations' => $validationErrors]);
    }

    /**
     * @param array $data
     * @param string $key
     * @param int $default
     * @return int
     */
    public static function getObjectId(array $data, string $key, $default = -1)
    {
        return isseT($data[$key]) && \ctype_digit($data[$key]) ? \intval($data[$key]) : $default;
    }

    public static function map($data, callable $callback = null): array
    {
        if(\is_callable($callback) === false) {
            $callback = function(CrowModel $model) { return $model->toMap(); };
        }

        $result = [];

        foreach($data as $entry) {
            $result[] = $callback($entry);
        }

        return $result;
    }
}
