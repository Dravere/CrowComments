<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments;


class Session
{
    const USER_ID_KEY = 'userId';

    public static function push($key, $value)
    {
        if(isset($_SESSION[$key])) {
            if(\is_array($_SESSION[$key])) {
                $_SESSION[$key][] = $value;
            }
            else {
                $_SESSION[$key] = [$_SESSION[$key], $value];
            }
        }
        else {
            $_SESSION[$key] = [$value];
        }
    }

    public static function get($key, $default = null)
    {
        if(!isset($_SESSION[$key])) {
            return $default;
        }

        return $_SESSION[$key];
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function delete($key)
    {
        unset($_SESSION[$key]);
    }

    public static function setUserId(int $userId)
    {
        self::set(self::USER_ID_KEY, $userId);
    }

    public static function getUserId(): int
    {
        return self::get(self::USER_ID_KEY, -1);
    }

    public static function deleteUserId()
    {
        self::delete(self::USER_ID_KEY);
    }

    public static function contains($key, $value)
    {
        if(isset($_SESSION[$key])) {
            if(\is_array($_SESSION[$key])) {
                return \in_array($value, $_SESSION[$key], true);
            }

            return $_SESSION[$key] === $value;
        }

        return false;
    }
}
