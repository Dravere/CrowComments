<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Models;


use FMGSoftware\CrowComments\Pagination;



class CrowModel extends \Model
{
    /**
     * @param Pagination|null $pagination
     * @return \ORMWrapper
     */
    public static function startQuery($pagination = null)
    {
        $orm = \Model::factory(static::class);
        if(isset($pagination)) {
            return $orm->limit($pagination->getPageSize())->offset($pagination->getPageOffset());
        }

        return $orm;
    }

    public static function exists(int $id): bool
    {
        return static::startQuery()->whereIdIs($id)->count() === 1;
    }

    /**
     * @return array
     */
    public function toMap()
    {
        return $this->asArray();
    }
}