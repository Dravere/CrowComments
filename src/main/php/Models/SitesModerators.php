<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Models;


/**
 * Class SitesModerators
 * @package FMGSoftware\CrowComments\Models
 * @property int id
 * @property int siteId
 * @property int userId
 * @property int entryTime
 * @method static SitesModerators|bool findOne($id=null)
 * @method static Array|\IdiormResultSet findMany()
 * @method static SitesModerators create($data = null)
 */
class SitesModerators extends CrowModel
{
    public static $_table = 'sites_moderators';

    const SITE_ID_COLUMN = 'siteId';
    const USER_ID_COLUMN = 'userId';

    public static function isModeratorForSite(int $userId, int $siteId)
    {
        return self::startQuery()
                ->whereEqual(self::SITE_ID_COLUMN, $siteId)
                ->whereEqual(self::USER_ID_COLUMN, $userId)
                ->count() === 1;
    }

    public static function makeModeratorForSite(int $userId, int $siteId)
    {
        $siteModerator = self::create();
        $siteModerator->siteId = $siteId;
        $siteModerator->userId = $userId;
        $siteModerator->entryTime = \time();
        return $siteModerator->save();
    }

    public static function removeModeratorForSite(int $userId, int $siteId)
    {
        $siteModerator = self::startQuery()
            ->whereEqual(self::SITE_ID_COLUMN, $siteId)
            ->whereEqual(self::USER_ID_COLUMN, $userId)
            ->findOne();

        if($siteModerator === false) {
            return true;
        }

        return $siteModerator->delete();
    }

    public function toMap()
    {
        // Not a good idea to map this table. This is only a meta table!
        return [];
    }
}
