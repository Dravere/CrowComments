<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Models;


use Respect\Validation\Validator as v;


/**
 * Class User
 * @package FMGSoftware\CrowComments\Models
 * @property int id
 * @property string email
 * @property string|null password
 * @property string|null name
 * @property int isAdmin
 * @property int recordTime
 * @property int|null registerTime
 * @property int|null editTime
 * @method static User|bool findOne($id = null)
 * @method static Array|\IdiormResultSet findMany()
 * @method static User create($data = null)
 */
class User extends CrowModel
{
    public static $_table = 'users';

    const EMAIL_COLUMN = 'email';
    const NAME_COLUMN = 'name';

    /**
     * @param string $email
     * @return bool|User
     */
    public static function findUserByEmail(string $email)
    {
        // Teach the IDE about types, the joy of dynamic typing...
        /** @var bool|User $user */
        $user = User::startQuery()->whereEqual(self::EMAIL_COLUMN, $email)->findOne();
        return $user;
    }

    /**
     * @param string $email
     * @return bool|int
     */
    public static function findOrCreate($email)
    {
        $user = self::findUserByEmail($email);
        if($user === false) {
            $user = User::create();
            $user->email = $email;
            $user->recordTime = \time();

            if($user->save() !== true) {
                return false;
            }
        }

        return $user->id;
    }

    public static function validateUserData(array $data, int $userId = -1)
    {
        $validator = v::key('email', v::stringType()->length(3, 255)->email())
            ->key('name', v::stringType()->length(3, 32)->addRule(self::createUserHandleValidator($userId)), false);

        $validator->assert($data);
    }

    public static function createUserHandleValidator(int $userId = -1)
    {
        $userHandleValidator = v::callback(function ($userHandle) use ($userId) {
            return \is_null($userHandle) || $userHandle === '' || User::validateUserHandle($userHandle, $userId);
        });

        $userHandleValidator->setTemplate('name is already used by a registered user!');
        return v::allOf($userHandleValidator, UserHandleRule::createUserHandleValidator());
    }

    private static function validateUserHandle(string $userHandle, int $userId = -1)
    {
        $user = self::startQuery()->whereEqual(self::NAME_COLUMN, $userHandle)->findOne();
        if($user !== false && \intval($user->id()) !== $userId) {
            return false;
        }

        return true;
    }

    public function toMap()
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,
            'isAdmin' => $this->isAdmin,
            'recordTime' => \intval($this->recordTime) * 1000,
            'registerTime' => \is_null($this->registerTime) ? null : \intval($this->registerTime) * 1000,
            'editTime' => \is_null($this->editTime) ? null : \intval($this->registerTime) * 1000
        ];
    }
}