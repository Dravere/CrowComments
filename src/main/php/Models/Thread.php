<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Models;


use Respect\Validation\Validator as v;


/**
 * Class Thread
 * @package FMGSoftware\CrowComments\Models
 * @property int id
 * @property string caption
 * @property string token
 * @property int siteId
 * @method static Thread|bool findOne($id=null)
 * @method static Array|\IdiormResultSet findMany()
 * @method static Thread create($data = null)
 */
class Thread extends CrowModel
{
    public static $_table = 'threads';

    const SITE_ID_COLUMN = 'siteId';
    const TOKEN_COLUMN = 'token';

    public static function validateThreadData($data, $threadId = null)
    {
        $validator = v::key('caption', v::stringType()->length(3, 255))
            ->key('token',
                v::stringType()
                    ->length(8, 255)
                    ->callback(Thread::class . '::validateUniqueToken', $threadId),
                false);

        $validator->assert($data);
    }

    public static function validateUniqueToken($token, $threadId = null): bool
    {
        return self::startQuery()
                ->whereEqual(self::TOKEN_COLUMN, $token)
                ->whereNotEqual(self::DEFAULT_ID_COLUMN, $threadId)
                ->count() === 0;
    }

    /**
     * @param string $token
     * @return Thread|bool
     */
    public static function getThreadByToken(string $token)
    {
        // Teach IDE about the right type here. Dynamic typed languages...
        /** @var Thread|bool $result */
        $result = Thread::startQuery()->whereEqual(self::TOKEN_COLUMN, $token)->findOne();
        return $result;
    }

    public static function generateUniqueToken()
    {
        for($i = 0; $i < 10; ++$i) {
            $token = \openssl_random_pseudo_bytes(16);
            if($token === false) {
                return false;
            }

            $token = \bin2hex($token);

            if(self::validateUniqueToken($token)) {
                return $token;
            }
        }

        return false;
    }

    public function toMap()
    {
        return [
            'id' => $this->id,
            'caption' => $this->caption,
            'token' => $this->token
        ];
    }
}