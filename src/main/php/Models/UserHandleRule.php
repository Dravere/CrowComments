<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments\Models;


use Respect\Validation\Validator as v;


/**
 * Class UserHandleRule
 * @package FMGSoftware\CrowComments\Models
 * @property int id
 * @property string name
 * @property string regex
 * @property string userMessage
 * @property string description
 * @property int entryTime
 * @method static UserHandleRule|bool findOne($id=null)
 * @method static Array|\IdiormResultSet findMany()
 * @method static UserHandleRule create($data = null)
 */
class UserHandleRule extends CrowModel
{
    public static $_table = 'user_handle_rules';

    public static function validateRuleData(array $data)
    {
        $validator = v::key('name', v::stringType()->length(1, 255))
            ->key('regex', v::stringType()->length(1, 255))
            ->key('userMessage', v::stringType()->length(0, 255))
            ->key('description', v::stringType()->length(0, 255));

        return $validator->assert($data);
    }

    /**
     * @return \Respect\Validation\Validator A validator
     */
    public static function createUserHandleValidator()
    {
        $validator = new v();

        /** @var UserHandleRule $rule */
        foreach(self::findMany() as $rule) {
            $validator->not(v::regex($rule->regex)->setTemplate('name rule violation: ' . $rule->userMessage));
        }

        return $validator;
    }

    public function validate($userHandle)
    {
        return \preg_match($this->regex, $userHandle) !== 0;
    }

    public function toMap()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'regex' => $this->regex,
            'userMessage' => $this->userMessage,
            'description' => $this->description,
            'entryTime' => $this->entryTime
        ];
    }
}
