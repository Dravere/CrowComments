<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Models;


use Respect\Validation\Validator as v;


/**
 * Class Comment
 * @package FMGSoftware\CrowComments\Models
 * @property int $id
 * @property string userHandle
 * @property string message
 * @property int threadId
 * @property int userId
 * @property int postTime
 * @property int|null editTime
 * @property int|null replyTo
 * @method static Comment|bool findOne($id=null)
 * @method static Array|\IdiormResultSet findMany()
 * @method static Comment create($data = null)
 */
class Comment extends CrowModel
{
    public static $_table = 'comments';

    const THREAD_ID_COLUMN = 'threadId';
    const USER_ID_COLUMN = 'userId';

    public static function validateCommentData(array $data, int $userId = -1)
    {
        $validator = v::key('userHandle', v::allOf(v::stringType(), v::length(3, 32), User::createUserHandleValidator($userId)))
            ->key('email', v::stringType()->email())
            ->key('message', v::stringType()->length(3, 32768));

        $validator->assert($data);
    }

    public static function validateCommentMessage($data)
    {
        v::key('message', v::stringType()->length(3, 32768))->assert($data);
    }

    public function toMap(): array
    {
        return [
            'id' => $this->id,
            'userHandle' => $this->userHandle,
            'message' => $this->message
        ];
    }
}