<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments\Models;


use Respect\Validation\Validator as v;


/**
 * Class Site
 * @package FMGSoftware\CrowComments\Models
 * @property int id
 * @property string caption
 * @property string url
 * @method static Site|bool findOne($id=null)
 * @method static Array|\IdiormResultSet findMany()
 * @method static Site create($data = null)
 */
class Site extends CrowModel
{
    public static $_table = 'sites';

    public static function validateSiteData($data)
    {
        $validator = v::key('caption', v::stringType()->length(3, 255))
            ->key('url',
                v::stringType()
                    ->filterVar(
                        FILTER_VALIDATE_URL,
                        FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED)
                    ->OneOf(
                        v::startsWith('http://', true),
                        v::startsWith('https://', true))
                    ->length(null, 255));

        $validator->assert($data);
    }

    public function toMap()
    {
        return [
            'id' => $this->id,
            'caption' => $this->caption,
            'url' => $this->url
        ];
    }
}