<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.


namespace FMGSoftware\CrowComments;


use Throwable;


class UnexpectedException extends \RuntimeException
{
    private $data;

    public function __construct(string $message = "", array $data = [], Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);

        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}
