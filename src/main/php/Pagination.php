<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

namespace FMGSoftware\CrowComments;


use Slim\Http\Request;


class Pagination
{
    public static function createFromRequest(Request $request)
    {
        $page = intval($request->getQueryParam('page', 0));
        $pageSize = intval($request->getQueryParam('pageSize', 20));

        return new Pagination($page, $pageSize);
    }

    private $page;
    private $pageSize;

    public function __construct($page = null, $pageSize = null)
    {
        $this->page = \is_null($page) ? 0 : intval($page);
        $this->pageSize = \is_null($pageSize) ? 20 : intval($pageSize);

        if($this->page < 0) {
            $this->page = 0;
        }

        if($this->pageSize <= 0) {
            $this->pageSize = 20;
        }
        else if($this->pageSize > 100) {
            $this->pageSize = 100;
        }
    }

    /**
     * @return int The current page.
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int The size of a page.
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return int The page offset calculated from the current page and page size.
     */
    public function getPageOffset(): int
    {
        return $this->page * $this->pageSize;
    }

    public function applyTo(\ORM $query)
    {
        return $query->limit($this->getPageSize())->offset($this->getPageOffset());
    }
}