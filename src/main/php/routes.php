<?php
// This file is part of CrowComments.
// It is licensed under the MIT license.
// You can find the license in the root folder of the project.

use FMGSoftware\CrowComments\Controllers\AdminController;
use FMGSoftware\CrowComments\Controllers\ApiThreadController;
use FMGSoftware\CrowComments\Controllers\AuthController;
use FMGSoftware\CrowComments\Middlewares\AdminMiddleware;
use FMGSoftware\CrowComments\Middlewares\DbTransactionMiddleware;
use FMGSoftware\CrowComments\Middlewares\UserMiddleware;


function initializeRoutes(\Slim\App $app) {
    $app->add(new \FMGSoftware\CrowComments\Middlewares\ExceptionHandlingMiddleware($app->getContainer()));

    $app->get('/', function (\Slim\Http\Request $request, \Slim\Http\Response $response) {
        return $response->withRedirect('/admin/', 302);
    });

    $dbTransactionMiddleware = new DbTransactionMiddleware();

    $app->group('/api', function() {
        $this->get('/threads/{threadToken}/comments', ApiThreadController::class . ':getComments');
        $this->post('/threads/{threadToken}/comments', ApiThreadController::class . ':createComment');
        $this->patch('/comments/{commentId}', ApiThreadController::class . ':updateComment');

        $this->post('/register', AuthController::class . ':register');
        $this->post('/login', AuthController::class . ':login');
        $this->post('/logout', AuthController::class . ':logout');

        $this->group('/ego', function() {
            $this->post('/update', AuthController::class . ':egoUpdate');
            $this->post('/password', AuthController::class . ':egoPassword');
            //$this->get('/ego/anonymize');
            //$this->get('/ego/delete');
        })->add(new UserMiddleware());
    })->add($dbTransactionMiddleware);

    $displayPage = function ($page) {
        return function($request, $response, $args = []) use ($page) {
            return $this->view->render($response, $page, $args);
        };
    };

    $app->get('/register', $displayPage('register.html.twig'));
    $app->get('/login', $displayPage('login.html.twig'));

    $app->group('/admin', function() use ($dbTransactionMiddleware, $displayPage) {
        $this->get('/', $displayPage('admin-index.html.twig'));
        $this->get('/sites', $displayPage('admin-sites.html.twig'));
        $this->get('/sites/{siteId}/threads', $displayPage('admin-threads.html.twig'));
        $this->get('/threads/{threadId}', AdminController::class . ':displayThread');
        $this->get('/comments', AdminController::class . ':displayComments');
        $this->get('/users', $displayPage('admin-users.html.twig'));

        $this->group('/api', function() {
            $this->get('/sites', AdminController::class . ':getSiteList');
            $this->get('/sites/{siteId}', AdminController::class . ':getSite');
            $this->get('/sites/{siteId}/threads', AdminController::class . ':getSiteThreadList');
            $this->get('/threads', AdminController::class . ':getThreadList');
            $this->get('/threads/{threadId}', AdminController::class . ':getThread');
            $this->get('/comments', AdminController::class . ':getCommentList');
            $this->get('/comments/{commentId}', AdminController::class . ':getComment');
            $this->get('/users', AdminController::class . ':getUserList');
            $this->get('/users/{userId}', AdminController::class . ':getUser');
            $this->get('/rules/userHandles', AdminController::class . ':getUserHandleRuleList');

            $this->post('/sites', AdminController::class . ':createSite');
            $this->post('/sites/{siteId}/threads', AdminController::class . ':createThread');
            $this->post('/rules/userHandles', AdminController::class . 'createUserHandleRule');

            $this->patch('/sites/{siteId}', AdminController::class . ':updateSite');
            $this->patch('/threads/{threadId}', AdminController::class . ':updateThread');
            $this->patch('/comments/{commentId}', AdminController::class . ':updateComment');
            $this->patch('/users/{userId}', AdminController::class . ':updateUser');
            $this->patch('/rules/userHandles/{ruleId}', AdminController::class . ':updateUserHandleRule');

            $this->delete('/sites/{siteId}', AdminController::class . ':deleteSite');
            $this->delete('/threads/{threadId}', AdminController::class . ':deleteThread');
            $this->delete('/comments/{commentId}', AdminController::class . ':deleteComment');
            $this->delete('/users/{userId}', AdminController::class . ':deleteUser');
            $this->delete('/rules/userHandles/{ruleId}', AdminController::class . 'deleteUserHandleRule');
        })->add($dbTransactionMiddleware);
    })->add(new AdminMiddleware());
}
