const webpack = require('webpack');
const UglifyWebpackPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require("clean-webpack-plugin");
const GitRevisionPlugin = require('git-revision-webpack-plugin');

exports.minifyJavaScript = () => ({
    optimization: {
        minimize: true,
        minimizer: [new UglifyWebpackPlugin({ sourceMap: true })],
    },
});

exports.clean = (paths, options=undefined) => ({
    plugins: [new CleanWebpackPlugin(Array.isArray(paths) ? paths : [paths], options)],
});

exports.attachRevision = () => ({
    plugins: [
        new webpack.BannerPlugin({
            banner: new GitRevisionPlugin().version(),
        }),
    ],
});