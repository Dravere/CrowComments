const path = require('path');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const parts = require('./webpack.parts');


const commonConfig = merge([
    {
        entry: {
            dependencies: './src/main/javascript/dependencies.js',
            index: './src/main/javascript/index.js',
            register: './src/main/javascript/register.js',
            login: './src/main/javascript/login.js',
            'admin-index': './src/main/javascript/admin-index.js',
            'admin-sites': './src/main/javascript/admin-sites.js',
            'admin-threads': './src/main/javascript/admin-threads.js',
            'admin-users': './src/main/javascript/admin-users.js'
        },
        output: {
            path: path.resolve(__dirname, '../../public/'),
            filename: "js/[name].bundle.js"
        },
        module: {
            rules: [{
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'clean-css-loader', {
                    loader: 'less-loader',
                    options: {
                        paths: [
                            path.resolve(__dirname, '../../node_modules/uikit/src/less')
                        ]
                    }
                }]
            }, {
                test: /\.vue$/,
                loader: 'vue-loader'
            }, {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }]
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js'
            },
            extensions: ['.wasm', '.mjs', '.js', '.vue', '.json']
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        name: 'commons',
                        chunks: 'initial',
                        minChunks: 2
                    }
                }
            }
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: 'styles/[name].css'
            }),
            new VueLoaderPlugin()
        ]
    },
    parts.clean(['js', 'styles'], {root: path.resolve(__dirname, '../../public/')}),
    parts.attachRevision()
]);

const productionConfig = merge([
    parts.minifyJavaScript()
]);

const developmentConfig = merge([{
    devtool: 'source-map',
    watch: true
}]);

module.exports = (env, argv) => {
    if(argv.mode === 'production') {
        return merge(commonConfig, productionConfig);
    }

    return merge(commonConfig, developmentConfig);
};